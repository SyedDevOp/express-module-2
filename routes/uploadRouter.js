const express = require('express');
const bodyParser = require('body-parser');
const authenticate =require('../authenticate');
const multer = require('multer');
const cors = require('./cors');

const storage = multer.diskStorage({          //modifying multer setting
    destination: (req, file, cb)=>{         //location where file will be stored
        cb(null, 'public/images');             // err and location     
    },
    filename: (req, file, cb)=>{
        cb(null, file.originalname)         //err and filename setting to original name
    }
});

const imageFileFilter = (req, file, cb)=>{
    if(!file.originalname.match(/\.(jgp|jpeg|png|gif)$/)){             //checking extension of the file using regular expression
        return cb(new Error('You cannot upload only image Files'), false);
    }   
    cb(null, true);                                             //image is ok and we allow it to be upload
};

const upload = multer({storage: storage, fileFilter: imageFileFilter});         //adding our configration into multer     

const uploadRouter = express.Router();
uploadRouter.use(bodyParser.json());

uploadRouter.route('/')
.options(cors.corsWithOptions, (req, res)=>{ res.statusCode(200);})     //checking from server is origin allowed
.get(cors.cors, (req, res, next)=>{
    res.statusCode = 403;
    res.end('GET operation not supported on /imageUpload');
})
.put(cors.corsWithOptions, (req, res, next)=>{
    res.statusCode = 403;
    res.end('put operation not supported on /imageUpload');
})
.delete(cors.corsWithOptions, (req, res, next)=>{
    res.statusCode = 403;
    res.end('DELETE operation not supported on /imageUpload');
})
.post(cors.corsWithOptions, authenticate.verifyUser, upload.single('imageFile') ,(req, res)=>{        //upload single means upload  single file from "FORM" field specifyed in the parameters
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(req.file);                                                 //sending information about uploaded file to user
    res.end('GET operation not supported on /imageUpload');
})
module.exports = uploadRouter;