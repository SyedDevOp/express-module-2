const express = require('express');
const bodyParser = require('body-parser');
const mongooser = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Promotions = require('../models/Promotions')

const promoRouter = express.Router();
promoRouter.use(bodyParser.json());

promoRouter.route('/')
.options(cors.corsWithOptions, (req, res)=>{ res.statusCode(200);})     //checking from server is origin allowed
.get(cors.cors, (req, res, next)=>{
    Promotions.find(req.query)
    .then((promotion)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
    },(err)=> next(err))
    .catch((err)=> next(err));
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res, next)=>{
    authenticate.verifyAdmin(req.user, next);
    Promotions.create(req.body)
    .then((promotion)=>{
        console.log('Promotion Created', promotion);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
    }, (err) => next(err))
    .catch((err)=>next(err));   
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res)=>{
    authenticate.verifyAdmin(req.user, next);
    res.statusCode = 403;
    res.end('put operation not Supported');
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next)=>{
    authenticate.verifyAdmin(req.user, next);
    Promotions.remove({})
    .then((resp)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err)=>next(err));
});
promoRouter.route('/:promoId')
.options(cors.corsWithOptions, (req, res)=>{ res.statusCode(200);})     //checking from server is origin allowed
.get(cors.cors, (req, res,next)=>{
    Promotions.findById(req.params.promoId)
    .then((promotion)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
    }, (err) => next(err))
    .catch((err)=>next(err));
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res)=>{
    authenticate.verifyAdmin(req.user, next);
    res.statusCode = 403;
    res.end('POST operation not upported on /promotions/' + req.params.promoId);
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res, next)=>{
    authenticate.verifyAdmin(req.user, next);
    Promotions.findByIdAndUpdate(req.params.promoId, {$set: req.body}, {new: true})
    .then((promotion)=>{
        console.log('Dish Updated', promotion);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
    }, (err) => next(err))
    .catch((err)=>next(err)); 
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next)=>{
    authenticate.verifyAdmin(req.user, next);
    Promotions.findByIdAndRemove(req.params.promoId)
    .then((promotion)=>{
        console.log('Dish Deleted', promotion);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
    }, (err) => next(err))
    .catch((err)=>next(err));
});
module.exports = promoRouter;