const express = require('express');
const cors = require('cors');
const app = express();

const whitelist = ['http://localhost:3000', 'https://localhost:3443', 'http://localhost:4200'];              //contains all the origin the server in willing to accept
var corsOptionDelegate = (req, callback) => {
    var corsOptions;
    if (whitelist.indexOf(req.header('Origin') !== -1)) {                                    //checking header for origin in header
        corsOptions = { origin: true };                                    //origin true means we accpet origin to be continue
    }
    else {
        corsOptions = { origin: false };
    }
    callback(null, corsOptions);
}

exports.cors = cors();
exports.corsWithOptions = cors(corsOptionDelegate);