const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors =  require('./cors');
const Leaders = require('../models/leaders');

const leadersRouter = express.Router();
leadersRouter.use(bodyParser.json());

leadersRouter.route('/')
.options(cors.corsWithOptions, (req, res)=>{ res.statusCode(200);})     //checking from server is origin allowed
.get(cors.cors, (req, res, next)=>{
    Leaders.find(req.query)
    .then((leaders)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(leaders);
    },(err)=> next(err))
    .catch((err)=> next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next)=>{
    authenticate.verifyAdmin(req.user, next);
    Leaders.create(req.body)
    .then((leader)=>{
        console.log('Leader Created', leader);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(leader);
    }, (err) => next(err))
    .catch((err)=>next(err));
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res)=>{
    authenticate.verifyAdmin(req.user, next);
    res.statusCode = 403;
    res.end('put operation not Supported');
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res)=>{
    authenticate.verifyAdmin(req.user, next);
    Leaders.remove({})
    .then((resp)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err)=>next(err));
});

leadersRouter.route('/:leaderId')
.options(cors.corsWithOptions, (req, res)=>{ res.statusCode(200);})     //checking from server is origin allowed
.get(cors.cors, (req, res, next)=>{
    Leaders.findById(req.params.leaderId)
    .then((leader)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(leader);
    }, (err) => next(err))
    .catch((err)=>next(err));
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res)=>{
    authenticate.verifyAdmin(req.user, next);
    res.statusCode = 403;
    res.end('POST operation not upported on /leaders/' + req.params.leaderId);
})
.put(cors.corsWithOptions,authenticate.verifyUser, (req, res, next)=>{
    authenticate.verifyAdmin(req.user, next);
    Leaders.findByIdAndUpdate(req.params.leaderId, {$set: req.body}, {new: true})
    .then((leader)=>{
        console.log('Dish Updated', leader);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(leader);
    }, (err) => next(err))
    .catch((err)=>next(err)); 
})
.delete(cors.corsWithOptions,authenticate.verifyUser, (req, res, next)=>{
    authenticate.verifyAdmin(req.user, next);
    Leaders.findByIdAndRemove(req.params.leaderId)
    .then((leader)=>{
        console.log('Dish Deleted', leader);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(leader);
    }, (err) => next(err))
    .catch((err)=>next(err));
});
module.exports = leadersRouter;