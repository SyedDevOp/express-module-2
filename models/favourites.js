const mongoose = require('mongoose');
const schema = mongoose.Schema;
const favouritesSchema = new schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    dishes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Dish'
    }]
}, {
    timestamps: true
});
var favourties = mongoose.model('Favourite', favouritesSchema);
module.exports = favourties;