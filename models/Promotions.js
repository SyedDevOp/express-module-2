const mongoose = require('mongoose');
const schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);    //this will load the new currency in mongoose
const Currency = mongoose.Types.Currency;

const promoSchema = new schema({
    name:{
        type: String,
        required: true,
        unique: true
    },
    image:{
        type: String,
        required: true
    },
    label:{
        type: String,
        required: true
    },
    price:{
        type: Currency,
        required: true
    },
    featured:{
        type: Boolean,
        default: false
    },
    description:{
        type: String,
        required: true
    }
});

var Promotions = mongoose.model('promotion', promoSchema);
module.exports = Promotions;