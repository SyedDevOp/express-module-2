const mongoose = require('mongoose');
const schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);    //this will load the new currency in mongoose
const Currency = mongoose.Types.Currency;

const commentSchema = new schema({
    rating:{
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:{
        type: String,
        required: true
    },
    author:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    } 
}, {timestamps: true
});

const dishSchema = new schema({     //creating a new schema for dishes
    name: {                         //specifying first type as name
        type: String,               //specifying its properties like name should be a string
        required: true,             //cannot be empty
        unique: true                //must be unique
    },
    image:{
        type: String,
        required: true
    },
    category:{
        type: String,
        required: true
    },
    label:{
        type: String,
        default: ''
    },
    price:{
        type: Currency,
        required: true,
        min: 0
    },
    featured:{
        type: Boolean,
        default: false
    },
    description:{
        type: String,
        required: true,
    },
    comments: [commentSchema]        //subdocument
},{
    timestamps: true                //mongoose will automatically add it
    });

    var Dishes = mongoose.model('Dish', dishSchema);
    module.exports = Dishes;