//<-------------------Headers start----------------------->
var createError = require('http-errors'); 
var express = require('express');
var path = require('path'); 
var cookieParser = require('cookie-parser');  //use to parse information from cookie into request
var logger = require('morgan');         // use to log information into server console
const mongoose = require('mongoose');   //use to access databse
const Dishes = require('./models/dishes');
var session = require('express-session');       //use to create a session
var FileStore = require('session-file-store')(session);     // file storage to save session data(optional to db)
var authenticate = require('./authenticate'); 
var passport = require('passport');         //use for authentication
var config = require('./config');           //contain information

//<-------------------Headers ends----------------------->

//<-------------------Inculusions start----------------------->
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dishRouter = require('./routes/dishRouter');
var promoRouter = require('./routes/promoRouter');
var leaderRouter = require('./routes/leadersRouter');
const uploadRouter = require('./routes/uploadRouter');
const favoriteRouter = require('./routes/favouriteRouter');
//<-------------------inclusions end----------------------->

var app = express();
app.all('*', (req, res, next)=>{          //for all request whether secure aur not
  if(req.secure)
    return next();
  else
    res.redirect(301, 'https://'+req.hostname+':'+app.get('secPort')+req.url);
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//<-------------------Database Connection start----------------------->

const url = config.mongoUrl;
const connect = mongoose.connect(url);
connect.then((db) => {
  console.log('Connected to the Server');
}, (err) => {
  console.log(err);
});
//<-------------------Database connection end----------------------->

//<-------------------Middleware start----------------------->
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser('12345-67890-09876-54321'));    //encrpty key/secret key

app.use(passport.initialize());

app.use(express.static(path.join(__dirname, 'public')));

//<-----------------Routers------------------------------>
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/dishes', dishRouter);
app.use('/promotions', promoRouter);
app.use('/leaders', leaderRouter);
app.use('/imageUpload', uploadRouter);
app.use('/favorites', favoriteRouter);
//<-------------------Middleware End----------------------->


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
